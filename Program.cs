﻿using System;

namespace WaterBoilingDemo
{
    class Program
    {
        static void Main()
        {
            #region Подготовка консоли

            Console.Title = "Лабораторная работа 1.3";
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();

            #endregion

            #region Константы

            // Коэффициент пересчёта 1 [л] = 0.0001 [м^3]
            const double cubicMetersInLiter = 0.001;
            // Плотность воды, кг/м^3
            const double densityOfWater = 1000;
            // Коэффициент полезного действия нагревательного элемента
            const double boilerEfficiency = 0.4;
            // Теплоёмкость воды, Дж/(кг*К)
            const double specificHeatCapacityOfWater = 4183;
            // Коэффициент пересчёта 1 [Дж] = 1 [Вт*с] = 1000/3600 [кВт*ч]
            const double J2kWh = 1 / 3.6e+6;
            // Стоимость одного киловатт-часа электроэнергии (Москва, 2017 год), руб
            const double costOfkWh = 5.24;

            #endregion

            #region Входные параметры

            double firstVolumeOfWater,
                secondVolumeOfWater,
                startTemperature,
                finishTemperatureOfFirstVolumeOfWater,
                finishTemperatureOfSecondVolumeOfWater;

            #endregion

            #region Ввод данных

            string userInput;
            bool isValidValue;
            do
            {
                Console.Write("Первый объём воды составляет (л): ");
                userInput = Console.ReadLine();
                isValidValue = double.TryParse(userInput, out firstVolumeOfWater) && firstVolumeOfWater > 0;
                if (!isValidValue)
                    Console.WriteLine(
                        $"Введеное значение ({firstVolumeOfWater}) не является корректным. Объём воды должен быть представлен положительным вещественным числом.");
            } while (!isValidValue);

            do
            {
                Console.Write("Второй объём воды составляет (л): ");
                userInput = Console.ReadLine();
                isValidValue = double.TryParse(userInput, out secondVolumeOfWater) && secondVolumeOfWater > 0;
                if (!isValidValue)
                    Console.WriteLine(
                        $"Введеное значение ({secondVolumeOfWater}) не является корректным. Объём воды должен быть представлен положительным вещественным числом.");
            } while (!isValidValue);

            do
            {
                Console.Write("Начальная температура обоих объёмов воды составляет (*C): ");
                userInput = Console.ReadLine();
                isValidValue = double.TryParse(userInput, out startTemperature);
                if (!isValidValue)
                    Console.WriteLine(
                        $"Введеное значение ({startTemperature}) не является корректным. Температура воды должена быть представлена вещественным числом.");
            } while (!isValidValue);

            do {
                Console.Write("Конечная температура первого объёма воды составляет (*C): ");
                userInput = Console.ReadLine();
                isValidValue = double.TryParse(userInput, out finishTemperatureOfFirstVolumeOfWater) && finishTemperatureOfFirstVolumeOfWater > startTemperature;
                if (!isValidValue)
                    Console.WriteLine(
                        $"Введеное значение ({finishTemperatureOfFirstVolumeOfWater}) не является корректным. Конечная температура воды должена быть представлена вещественным числом, большим {startTemperature}.");
            } while (!isValidValue);

            do
            {
                Console.Write("Конечная температура первого объёма воды составляет (*C): ");
                userInput = Console.ReadLine();
                isValidValue = double.TryParse(userInput, out finishTemperatureOfSecondVolumeOfWater) && finishTemperatureOfSecondVolumeOfWater > startTemperature;
                if (!isValidValue)
                    Console.WriteLine(
                        $"Введеное значение ({finishTemperatureOfSecondVolumeOfWater}) не является корректным. Конечная температура воды должена быть представлена вещественным числом, большим {startTemperature}.");
            } while (!isValidValue);

            #endregion

            #region Вычисления

            double massOfFirstVolumeOfWater = firstVolumeOfWater * cubicMetersInLiter * densityOfWater;
            double massOfSecondVolumeOfWater = secondVolumeOfWater * cubicMetersInLiter * densityOfWater;

            double temperatureDifferenceForFirstVolumeOfWater =
                finishTemperatureOfFirstVolumeOfWater - startTemperature;
            double temperatureDifferenceForSecondVolumeOfWater =
                finishTemperatureOfSecondVolumeOfWater - startTemperature;

            double energyForBoilingFirstVolumeOfWater = specificHeatCapacityOfWater * massOfFirstVolumeOfWater * temperatureDifferenceForFirstVolumeOfWater;
            double energyForBoilingSecondVolumeOfWater = specificHeatCapacityOfWater * massOfSecondVolumeOfWater * temperatureDifferenceForSecondVolumeOfWater;

            double totalBoilingEnergy = energyForBoilingFirstVolumeOfWater + energyForBoilingSecondVolumeOfWater;
    
            double totalElectricalEnergy = totalBoilingEnergy / boilerEfficiency;

            double totalEnergyInkWh = totalElectricalEnergy * J2kWh;

            #endregion

            #region Результат

            double totalPrice = totalEnergyInkWh * costOfkWh;

            #endregion

            #region Вывод результатов

            Console.WriteLine($"На нагревание\n\t{firstVolumeOfWater} л воды от {startTemperature} *C до {finishTemperatureOfFirstVolumeOfWater} *C;\n\t{secondVolumeOfWater} л воды от {startTemperature} *C до {finishTemperatureOfSecondVolumeOfWater} *C\nбыло потрачено {totalPrice:0.00} рублей.");

            #endregion
        }
    }
}
